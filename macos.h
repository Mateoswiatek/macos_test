#ifndef MAC_OS_H
#define MAC_OS_H
// Mateusz Świątek
/** Jako punkt z aktywnosci można zaimplementować klasę `MacOs` analogicznie jak dla pozostałych systemów
    Jako kolejny dodatkowy punkt z aktywności - jak się utworzy analogiczne testy
    Najlepiej jak wrzucisz implementacje do swojego repozytorium, ale proszę pokazać na zajęciach (nie zauważe jak mi ktoś nie pokaże, że zrobił)
**/
#include <string>
#include <vector>

#include <cstdint>
#include <utility> // std::pair
using namespace std;

class User;

class Macos
{
    string system_ = "Monterey"; // https://support.apple.com/pl-pl/HT201260
    string version_="12.6.3";
    static size_t activated_systems_;
    vector<User> users_;
public:
    typedef wchar_t przezwisko; //182 linijka w tescie

    string system_info();
    Macos();
    Macos(string, string);
    static size_t activated_systems();
    int add_user(string, string);
    User user(int);
    string user_home_directory(int);
};

#endif // MAC_OS_H


